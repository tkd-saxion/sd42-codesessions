import pyglet, math

class Dot():
    RADIUS = 3
    def __init__(self, x, y, opacity=255):
        self.x = x
        self.y = y
        self.opacity = opacity

    def update(self, game):
        self.opacity -= 1
        # if self.opacity <= 0:
        #     game.__trail.remove(self)

    def draw(self):
        # self.opacity -= 1
        circle = pyglet.shapes.Circle(self.x, self.y, Dot.RADIUS, color=(255,255,255,self.opacity))
        circle.draw()

class Plane():

    def __init__(self, x, y):
        self.__graphics = []
        self.__x = x
        self.__y = y
        self.__altitude = 2000
        self.__heading = 270
        self.__speed = 1
        self.__trail = []
        self.__square = pyglet.shapes.BorderedRectangle(self.__x, self.__y, 10, 10, border=3, border_color=(0,255,0,100), color=(0,0,0))
        self.__square.anchor_x = self.__square.anchor_y = 5
        self.__line = pyglet.shapes.Line(self.__x + 5, self.__y + 5, 0, 0)
        self.__calc_end_of_line()
        self.__tick = 0

    def set_speed(self, speed):
        self.__speed += speed

    def __calc_end_of_line(self):
        self.__line.x2 = self.__x + self.__square.anchor_x + 20 * math.sin(math.radians(self.__heading))
        self.__line.y2 = self.__y + self.__square.anchor_y + 20 * math.cos(math.radians(self.__heading)) 
    
    def move(self):
        if self.__tick % 20 == 0:
            self.trail()

        self.__x += self.__speed * math.sin(math.radians(self.__heading))
        self.__y += self.__speed * math.cos(math.radians(self.__heading))

        self.__line.x = self.__square.x = self.__x
        self.__line.y = self.__square.y = self.__y

    def heading(self, degrees):
        self.__heading += degrees
        self.__calc_end_of_line()

    def trail(self):
        if len(self.__trail) >= 5:
            self.__trail.pop(0)
        
        dot = Dot(self.__x, self.__y)
        self.__trail.append(dot)


    def draw(self):
        self.__tick += 1


        self.__line.draw()
        self.__square.draw()
        print(f"Line (x: {self.__line.x}, y: {self.__line.y}) - (x: {self.__line.x2}, y: {self.__line.y2})")
        for dot in self.__trail:
            dot.update(self)
            dot.draw()

        

    def __str__(self):
        return f"Position ({self.__x},{self.__y}), HEAD({self.__heading})"


class Game(pyglet.window.Window):

    def __init__(self):
        super().__init__()
        self.plane = Plane(self.width // 2, self.height // 2)
        self.degrees_change = 1

    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.LEFT:
            self.degrees_change = -1
        elif symbol == pyglet.window.key.RIGHT:
            self.degrees_change = 180
        elif symbol == pyglet.window.key.UP:
            self.plane.set_speed(1)


    def on_draw(self):
        self.clear()
        if self.degrees_change == 180:
            self.plane.heading(self.degrees_change)
        self.degrees_change = 0
        self.plane.move()
        self.plane.draw()
        print(self.plane)

if __name__ == '__main__':
    game = Game()
    pyglet.app.run()