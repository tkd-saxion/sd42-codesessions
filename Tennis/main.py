import random

#Players
PLAYERS = ['Tibor Kleinman-Derksen', 'Björn Borg']

#Possible set-points
POSSIBLE_POINTS = [0, 15, 30, 40, 'A', 'WON']
#sets possibilty's 0-0, 15-0, 30-40, 30-A (Advance) 

#set points with the index of POSSIBLE_POINTS
points_index = [0,0] 

#Games 0,0, 2,0, First reaches 6 points with a advance of 2 points wins
game = [0,0]

def score_game(player) -> bool:
    
    if player == 0:
        enemy = 1
    else:
        enemy = 0

    game[player] += 1

    different_points = game[player] - game[enemy]

    if game[player] >= 6 and different_points > 1:
        return True

def score(player) -> bool:
    '''When a player scores'''
    if player == 0:
        enemy = 1
    else:
        enemy = 0

    #0, 1, 2, 3, 4
    points_index[player] += 1

    different_index = points_index[player] - points_index[enemy]

    #If players scores again and has index 4
    # if points[player] == 4 or points[player] == 3:
    if points_index[player] == 4 and points_index[enemy] == 4:
        points_index[enemy] = points_index[player] = 3


    if points_index[player] >= 4 and different_index >= 2:
        return True


def sim_play():
    while True:
        player_scores = random.randint(0, 1)
        
        set_won = score(player_scores)

        if not set_won:
            print(f"Score {POSSIBLE_POINTS[points_index[0]]} - {POSSIBLE_POINTS[points_index[1]]}")

        if set_won:
            # Reset the points 
            points_index[0] = 0
            points_index[1] = 0
            #Wich player wins the set
            print(f"Player {PLAYERS[player_scores]} set-win.")
            #Change the game score
            game_won = score_game(player_scores)
            print(f"{PLAYERS[0]} {game[0]} - {game[1]} {PLAYERS[1]}")
            if game_won:
                print(f"GAME_SET_MATCH: {PLAYERS[player_scores]} wins the game.")
                break
        
    
if __name__ == '__main__':
    sim_play()