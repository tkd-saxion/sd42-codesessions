def input_string(prompt : str) -> str:
    '''Ask user for string, Return input otherwise 
        string length == 0, Return None'''
    user_input = input(prompt)

    if len(user_input) == 0:
        return None
    
    return user_input

def input_int(prompt : str) -> int:
    '''Returns int otherwise a None'''
    user_input = input_string(prompt)
    if user_input and user_input.isdigit():
            return int(user_input)

    return None

def input_range(prompt : str, minimum : int, maximum: int  = 10 ) -> int:
    '''Returns a number in a range, or a None'''
    user_input = input_int(prompt)
    if user_input and user_input >= minimum and user_input <= maximum:
        return user_input

    return None