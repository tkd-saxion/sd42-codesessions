import os
header = {}
list_of_companies = [] #[{company: Saxion},{company: Apple} ]

def read_company(filename: str, headers) -> list:
    list_of_company = []
    try:
        with open(filename, 'r') as file_reader:
            #for every line
            for line in file_reader.readlines():
                complete_company = {}
                company = []
                #splits : header, description -> list
                result, others = line.strip('\n').split('" ')
                company.append(result.lstrip('"'))
                company.extend(others.split(" "))

                for index, head in enumerate(headers.keys()):
                    complete_company[head] = company[index]
                
                list_of_company.append(complete_company)

    except FileNotFoundError:
        return False
    return list_of_company


def read_headers(filename :  str) -> dict:
    #param: filename of the headers
    #param: header dict
    headers = {}
    #open file
    try:
        with open(filename, 'r') as file_reader:
            #for every line
            for line in file_reader.readlines():
                #splits : header, description -> list
                keyword, description = line.strip('\n').split(': ')
                headers[keyword] = description
    except FileNotFoundError:
        return False
    return headers

def write_to_file(filename : str, datalist : list, delimiter = ';'):
    #param filename (output.csv)
    #param list of data
    #param delimiter (, ; : ><)

    with open(filename, 'w') as file_writer:
        for index, key in enumerate(datalist[0].keys()):           
            result_line = f'{str(key).lower()}'

            if index != len(datalist[0].keys()) - 1:
                result_line += f'{delimiter}'
            else:
                result_line += '\n'
            
            file_writer.write(result_line)

        for item in datalist:
            #dict
            for index, value in enumerate(item.values()):
                if index == len(item.values()) - 1:
                    file_writer.write(f'{value}\n')
                else:
                    file_writer.write(f'{value}{delimiter}')


def main():
    print("These are the companies")
    headers = read_headers('companies.info')
    list_of_companies = read_company('companies.data', headers)

    write_to_file('output.csv', list_of_companies)

if __name__ == '__main__':
    main()