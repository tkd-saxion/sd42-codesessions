import random
import helper

clubs = ["FC-Twente","Feyenoord", "Heracles","Vogido","Willem II","Vitesse","Roda JC","ADO","FC Groningen"]
matches = [] # [["Ajax","Feyenoord"],["Heracles","Vogido"]]
# match = {
#     'thuis': 'Ajax',
#     'uit': 'Feyenoord',
#     'score_thuis': 0,
#     'score_uit': 0
# }
#scores = [] # [[0,5],[1,1]]
bets = [] #["B","C"]
MIN = 0
MAX = 10

def make_match(clubs):

    #Select first club
    club_a = random.choice(clubs)
    clubs.remove(club_a)

    #Select second club
    club_b = random.choice(clubs)
    clubs.remove(club_b)

    match = {
        'thuis': club_a,
        'uit': club_b,
        'score_thuis': 0,
        'score_uit': 0
    }

    return match

def make_score():
    score = []
    for _ in range(2):
        score.append(random.randint(MIN,MAX))

    #score = [random.randint(MIN, MAX) for _ in range(2)]
    return score

def show_match(match, show_score = False, bet = False):
    if show_score:
        print(f"{match['thuis']} {match['score_thuis']} - {match['score_uit']} {match['uit']}")
    else:
        print(f"(A) {match['thuis']} - {match['uit']} (B)")
    
    if bet:
        if check_bet(match, bet):
            print("Winner!")
        else:
            print("!!!! LOSER !!!!")

def check_bet(score, bet):
    return bet == check_score(score)

def check_score(match):
    if match['score_thuis'] > match['score_uit']:
        return "A"
    elif match['score_thuis'] < match['score_uit']:
        return "B"
    else:
        return "C"

while True:

    #Loop thru clubs when there are minimum 2 available
    while len(clubs) > 1:
        match = make_match(clubs)
        matches.append(match)

    #Generate scores
    for match in matches:
        scores = make_score()
        match['score_thuis'] = scores[0]
        match['score_uit'] = scores[1]
        # scores.append(make_score())
    
    #Make your bets
    for match in matches:
        show_match(match)
        bet = helper.input_in_list("Doe een gokje (C voor gelijkspel)? ", \
            ['A','B','C'])
        if bet:
            bets.append(bet)
        else:
            print("Incorrecte weddenschap")
            bets.append("ONGELDIG")

    #Show results with bets
    for i, match in enumerate(matches):
        show_match(match, True, bets[i])

    #Exit application
    quit()