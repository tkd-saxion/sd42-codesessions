# X O
# Controle 
#  3 op één rij
#  Diagonaal
#  3 verticaal

MARKERS = ['X', 'O']
players_list = []
board = [1,2,3,4,5,6,7,8,9]

def ask_name():
    name = input("Wat is je naam? ")
    return name

def ask_position(player_name):
    while True:
        position = input(f"{player_name}: welke positie wil jij je marker plaatsen: ")
        try:
            position = int(position)
            break
        except:
            print("Dit is geen valide getal.")
            continue

    return position - 1

players_list.append(ask_name())
players_list.append(ask_name())

def print_board(board):
    index = 0
    for row in range(3):
        for column in range(3):
            print(f' {board[index]} ', end='')
            index += 1
        print()

turns = 0

def winner(board):

    for row in range(3):
        if board[row] == board[row+1] == board[row+2]:
            return board[row]
    
    for col in range(3):
        if board[col] == board[col+3] == board[col+6]:
            return board[col]
    
    if board[0] == board[4] == board[8]:
        return board[0]
    
    if board[2] == board[4] == board[6]:
        return board[2]
    
    return False


def draw(board):
    #Er zijn geen cijfers meer op het board
    for number in board:
        if type(number) == int:
            return False
    return True

while True:
    for index, marker in enumerate(MARKERS):

        positie = ask_position(players_list[index])
        if board[positie] in MARKERS:
            print("Beurt overgeslagen, veld is al gebruikt!")
            continue
            
        board[positie] = marker
        
        print_board(board)

        winner_is = winner(board)
        if winner_is:
            print(winner_is)
            index_of_marker = MARKERS.index(winner_is)
            print(f'{players_list[index_of_marker]} has won the game!')
            exit()
        
        if draw(board):
            print("It is a draw!")
            exit()