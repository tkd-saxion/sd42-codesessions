import pygame
import random

COLOR_WHITE = (255,255,255)
COLOR_BLACK = (0,0,0)

SCREEN_WIDTH = 1200
SCREEN_HEIGTH = 800

BALL_RADIUS = 10
PAD_HEIGHT = 150

FPS = 60

score = [0,0]

pygame.init()

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGTH))

#Sound
ping = pygame.mixer.Sound('ping.wav')

#Text
font = pygame.font.Font('Sono-Medium.ttf', 72)
text = font.render(f'{score[0]} - {score[1]}', True, COLOR_WHITE)
textRect = text.get_rect()
textRect.x = SCREEN_WIDTH /2 - textRect.width / 2
textRect.y = SCREEN_HEIGTH / 2 - textRect.height /2

ball = pygame.sprite.Sprite()
ball.image = pygame.Surface([BALL_RADIUS*2,BALL_RADIUS*2])
ball.image.fill(COLOR_BLACK)
pygame.draw.circle(ball.image, (0,255,0), [BALL_RADIUS, BALL_RADIUS], BALL_RADIUS)
ball.rect = ball.image.get_rect()
ball.rect.x = 0
ball.rect.y = SCREEN_HEIGTH/2

#Pad A
pad_a = pygame.sprite.Sprite()
pad_a.image = pygame.Surface([10, PAD_HEIGHT])
pad_a.image.fill(COLOR_WHITE)
pad_a.rect = pad_a.image.get_rect()
pad_a.rect.x = 10
pad_a.rect.y = SCREEN_HEIGTH / 2 - PAD_HEIGHT / 2

#Pad B
pad_b = pygame.sprite.Sprite()
pad_b.image = pygame.Surface([10, PAD_HEIGHT])
pad_b.image.fill(COLOR_WHITE)
pad_b.rect = pad_b.image.get_rect()
pad_b.rect.x = SCREEN_WIDTH - 20
pad_b.rect.y = SCREEN_HEIGTH/2 - PAD_HEIGHT / 2

sprites = pygame.sprite.Group()
sprites.add(ball)
sprites.add(pad_a)
sprites.add(pad_b)
clock = pygame.time.Clock()

def set_speed():
    ball_speed_x = random.randint(5, 20)
    ball_speed_y = random.randint(-4, 8)
    return ball_speed_x, ball_speed_y

def reset_ball():
    ball.rect.x = SCREEN_WIDTH/2
    ball.rect.y = SCREEN_HEIGTH/2
    return set_speed()

ball_speed_x, ball_speed_y = reset_ball()

while True:

    ball.rect.x += ball_speed_x

    

    ball.rect.y += ball_speed_y

    #Collision Detection Ball with screen
    if ball.rect.bottom >= SCREEN_HEIGTH or ball.rect.top <= 0:
        ball_speed_y = ball_speed_y * -1
    elif ball.rect.right <= 0:
        score[1] += 1
        ball_speed_x, ball_speed_y =reset_ball()
    elif ball.rect.left >= SCREEN_WIDTH:
        score[0] += 1
        ball_speed_x, ball_speed_y = reset_ball()
        ball_speed_x = ball_speed_x * -1

    if pygame.sprite.collide_mask(pad_a, ball) or pygame.sprite.collide_mask(pad_b, ball):
        ping.play()
        ball_speed_x = ball_speed_x * -1    

    if ball_speed_x < 0:
        if ball.rect.y <= pad_a.rect.y + PAD_HEIGHT / 2:
            pad_a.rect.y -= 3
        elif ball.rect.y + BALL_RADIUS*2 >= pad_a.rect.y + PAD_HEIGHT / 2:
            pad_a.rect.y += 3
        elif ball.rect.y >= pad_a.rect.y and ball.rect.y + BALL_RADIUS * 2 <= pad_a.rect.y + PAD_HEIGHT:
            pad_a.rect.y += 3
        # pad_a.rect.y = ball.rect.y - PAD_HEIGHT / 2
    elif ball_speed_x >= 0:
        if ball.rect.y <= pad_b.rect.y + PAD_HEIGHT / 2:
            pad_b.rect.y -= 3
        elif ball.rect.y > pad_b.rect.y + PAD_HEIGHT / 2:
            pad_b.rect.y += 3
        elif ball.rect.y >= pad_b.rect.y and ball.rect.y + BALL_RADIUS * 2 <= pad_b.rect.y + PAD_HEIGHT:
            pad_b.rect.y += 3

    #Check if pad a and b are not outside screen
    if pad_a.rect.y + PAD_HEIGHT >= SCREEN_HEIGTH:
        pad_a.rect.y = SCREEN_HEIGTH - PAD_HEIGHT
    elif pad_a.rect.y <= 0:
        pad_a.rect.y = 0
    
    if pad_b.rect.y + PAD_HEIGHT >= SCREEN_HEIGTH:
        pad_b.rect.y = SCREEN_HEIGTH - PAD_HEIGHT
    elif pad_b.rect.y <= 0:
        pad_b.rect.y = 0

    screen.fill(COLOR_BLACK)
    text = font.render(f'{score[0]} - {score[1]}', True, COLOR_WHITE)
    screen.blit(text, textRect)

    sprites.draw(screen)

    sprites.update()
    
    pygame.display.flip()

    clock.tick(FPS)

pygame.quit()
