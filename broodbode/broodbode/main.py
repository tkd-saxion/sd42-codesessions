import requests
import json
import os
URI = "https://bestellen.broodbode.nl/v2-2/pccheck/7511JL/2022-12-16/bezorgen/8"
FILE = "broodapiresult.json"

#If the json file not exist, call the URI
if not os.path.exists(FILE):
    result = requests.api.get(URI)
    with open('broodapiresult.json', 'w') as file_writer:
        json.dump(result.json(), file_writer)

class BreadType():
    """Type of Bread

    """
    def __init__(self, id : int, name : str, active : int, surcharge : int):
        """Constructor of Breadtype

        Args:
            id (int): id of the breadtype, is used by Spread class
            name (str): name of the breadtype
            active (int): is the breadtype available. Will be parsed to a boolean
            surcharge (int): Extra charge for the breadtype
        """
        self.id = id
        self.name = name
        self.active = bool(active)
        self.surcharge = surcharge

    def __str__(self):
        """String repesentation of the object

        Returns:
            string : Name and id of the product
        """
        return f'<{self.name} ({self.id})>'

class Spread():
    """The spread that can be used on certain breadtypes
    """
    def __init__(self, id : int, title: str, description: str, price : float, breadtypes: list):
        """_summary_

        Args:
            id (int): id of the spread (product)
            title (str): name of the spread
            description (str): description of the spread
            price (float): price, will be cast to int in euro cents
            breadtypes (list): list of breadtype id's
        """
        self.id = id
        self.title = title
        self.description = description
        self.price = int(price * 100)
        self.breadtypes = breadtypes
    
    def get_price(self):
        """Get the price in a float

        Returns:
            float: calculated euro cents to normal euro with cents
        """
        return float(self.price/100)

    def __str__(self):
        """Stringify object

        Returns:
            string: Id, description and price (euro, float, 2 decimals)
        """
        return f'<{self.id} {self.description} - \N{euro sign} {self.get_price():.2f}>'


#A dict an list of the retrieved items
breadtypes = {}
spreads = []

#Read the json local file to make a object of it.
with open(FILE, 'r') as file_reader:
    content = file_reader.read()
    content = json.loads(content)

    #Iterate thru breadtypes and add them to a dict (key = id, value = obj)
    for breadtype in content['breadtypes']:
        btype = BreadType(breadtype['id'], breadtype['name'], breadtype['active'], breadtype['surcharge'])
        breadtypes[btype.id] = btype

    #Get all category's where the name is 'Broodjes'
    spread_category = [x['id'] for x in content['categories'] if x['categorie'] == 'Broodjes']
    
    #Iterate thru the products and filter out the spreads, make objects and add them to a list.
    for spread in content['products']:
        if spread['categorie_id'] in spread_category:
            spread_obj = Spread(spread['id'], spread['title'], spread['subtitle'], spread['price'], json.loads(spread['breadtypes']))
            spreads.append(spread_obj)

#Show all breadtypes
for breadtype in breadtypes:
    print(breadtype)

#Show all spreads, with the matching breadtypes
for spread in spreads:
    print(spread)
    for typ in spread.breadtypes:
        print(f' - {breadtypes[typ]}')