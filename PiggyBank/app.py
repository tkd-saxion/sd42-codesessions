from flask import Flask, render_template, request, redirect
import os, dotenv, csv

dotenv.load_dotenv()

app = Flask(__name__)

app.config['TEMPLATES_AUTORELOAD'] = True
app.config['ENV_PROD'] = os.getenv('ENV')

transactions = []

def read_transactions(filename):
    transactions = []
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for transaction in reader:
            transactions.append(transaction)
    return transactions

def save_transactions(filename, transactions):
    with open(filename, "w+", newline='') as csvfile:
        writer = csv.DictWriter(csvfile, transactions[0].keys())
        writer.writeheader()
        for trans in transactions:
            writer.writerow(trans)

@app.route('/')
def index():
    print(os.getenv('transaction_file'))
    transactions = read_transactions(os.getenv('transaction_file'))
    return render_template('list_transactions.html', ENV=app.config['ENV_PROD'], transactions = transactions)

@app.route('/transaction')
def transaction():
    return render_template('new_transaction.html', ENV=app.config['ENV_PROD'])

@app.route('/transaction', methods=["POST"])
def submit_transaction():
    transactions = read_transactions(os.getenv('transaction_file'))
    new_transaction = {
        'Description': request.form['description'],
        'Amount':request.form['amount'],
        'IO':request.form['io'],
        'Datetime': '2023-01-25 10:01:00',
        'Category': 'NOT DEFINED'
    }

    transactions.append(new_transaction)

    save_transactions(os.getenv('transaction_file'), transactions)
    
    return redirect('/')

if __name__ == "__main__":
    app.run()
    